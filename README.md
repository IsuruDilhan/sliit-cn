# hackathon-kickstarter

# Usage

```
git clone https://bitbucket.org/rajikaimal/hackathon-kickstarter
cd hackathon-kickstarter
npm install
npm i -g nodemon
npm i -g getstorybook
npm run dev
```

To run react-storybook

```
npm run storybook
```