import React from 'react';
import {GridList, GridTile} from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import DatePicker from 'material-ui/DatePicker';
import TextField from 'material-ui/TextField';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import {List, ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import 'whatwg-fetch';

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    marginLeft: 20,
  },
  gridList: {
    width: 800,
    height: 1000,
    overflowY: 'auto',
    marginBottom: 24,
  },
};

const style1 = {
  margin: 40,
};

const style2 = {
	margin: 50,
	width: 250,
	padding: 20,
};

const tilesData = [
  {
    img: 'http://images.skymetweather.com/themes/skymet/images/gallery/toplists/Top-Not-to-miss-food-items-in-Monsoon/4.jpg',
    title: 'Breakfast',
    price: '100.00',
    qty: '10',
  },
  {
    img: 'http://www.eonline.com/eol_images/Entire_Site/20151016/rs_560x415-151116111016-560.chili.cm.111615.jpg',
    title: 'Tasty burger',
    price: 'pashminu',
    qty: '10',
  },
  {
    img: 'http://www.3ders.org/images2015/experts-predict-3d-printed-customised-food-items-to-rule-the-industry-in-next-20-years-6.jpg',
    title: 'Pizza',
    price: 'Danson67',
    qty: '10',
  },
  {
    img: 'http://a57.foxnews.com/global.fncstatic.com/static/managed/img/fn2/travel/876/493/hotdogsiosiod.jpg?ve=1&tl=1',
    title: 'submarine',
    price: 'fancycrave1',
    qty: '10',
  },
  {
    img: 'http://image6.buzzintown.com/files/article/upload_22000/upload_original/449310-five-food-items-vegetarians-dont-like.jpg',
    title: 'Vegitable Rotti',
    price: 'Hans',
    qty: '10',
  },
  {
    img: 'http://www.careongo.com/riseup/wp-content/uploads/2015/07/Hyderabadi-Mutton-Biryani-Recipe.jpg',
    title: 'Chicken Rice',
    price: 'fancycravel',
    qty: '10',
  },
  {
    img: 'https://a2ua.com/juice/juice-006.jpg',
    title: 'Soft drinks',
    price: 'jill111',
    qty: '10',
  },
  {
    img: 'http://www.monikaicecream.com/img/gallery/fruit%20salad/img3.jpg',
    title: 'Fruit Salade',
    price: 'BkrmadtyaKarki',
    qty: '10',
  },
];

export class ItemList extends React.Component{
	  constructor(props,contex) {    
	    super(props,contex);
	    this.state = {
	            open: false,
	    };
	  };

	  state = {
	    open: false,
	  };

	  handleOpen = () => {
	    this.setState({open: true});
	  };

	  handleClose = () => {
	    this.setState({open: false});
	  };

	  _calcItemPrice = () => {
	  	let val = true;
	  	let qty = this.refs.qtyBox.value;

	  	/*if(validateStatusText(qty).error) {
	      this.setState({
	        qty: validateStatusText(qty).error
	      });
	      val = false;
	    } 
	    else {

	        fetch('/orderItem',{
	        	 	method: 'POST',
  					body: 
	        })
			  .then(function(response) {
			    return response.text()
			  }).then(function(body) {
			    document.body.innerHTML = body
			  })
			  .catch(function(err) {

			  });

	      this.setState({
	        qty: ''
	      });
	    }
	    this.clearText();*/
	  };

	  clearText() {
	    document.getElementById('qty').value = "";
	  };

	  _calcNetPrice = () => {

	  };

	  _proceed = () => {

	  };

	render() {
		const actions = [
	      <FlatButton
	        label="Add to cart"
	        primary={true}
	        keyboardFocused={true}
	        onTouchTap={this.handleClose}
	      />,
	    ];

		return (
			<div className="column">
				<div style={styles.root} className="col-lg-8">
				    <GridList
				      cellHeight={200}
				      style={styles.gridList}
				    >
				      <Subheader><h2>Food Items</h2></Subheader>
				      {tilesData.map((tile) => (
				        <GridTile
				          key={tile.img}
				          title={tile.title}
				          subtitle={<span>by <b>{tile.price}</b></span>}
				          actionIcon={<IconButton onClick={this.handleOpen}><StarBorder color="white" /></IconButton>}
				        >
				          <img src={tile.img} />
				        </GridTile>
				      ))}
				    </GridList>
				    <Dialog
			          title="Add to Cart"
			          actions={actions}
			          modal={false}
			          open={this.state.open}
			          onRequestClose={this.handleClose}
			        >
			          <TextField hintText="Quantity" floatingLabelText="Quantity" multiLine={false} ref="qtyBox" className="col-md-4"/>
			          <label className="col-md-8">Price: Rs 0.00</label>
			        </Dialog>
			  	</div>
			  	<div className="col-lg-3">
			  		<Card style={style2}>
					    <CardHeader
					      title="My Order"
					      actAsExpander={true}
					    />
					    <CardText>
					    	<List>
						        <ListItem
						          primaryText="submarine"
						          secondaryText="Quantity: 2"
						        />
						      </List>
					    </CardText>
					    <CardText>
					    	Net price : 0.00
					    </CardText>
					    <CardActions>
					    	<RaisedButton label="Proceed" primary={true} style={style1} />
					    </CardActions>
					</Card>
			  	</div>
			</div>
			);
	}
}
